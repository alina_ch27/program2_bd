﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace Mission_1
{

    public partial class Form1 : Form
    {
        private string ConnectionString = global::Mission_1.Properties.Settings.Default.YchetConnectionString;

        public Form1()
        {
            InitializeComponent();
        }
        private void AppdateSQL()
        {
            SqlConnection sqlc = new SqlConnection();
            sqlc.ConnectionString = ConnectionString;
            sqlc.Open();

        }


        private string connectionstring = global::Mission_1.Properties.Settings.Default.YchetConnectionString;
        private string kadrTableName = "Otdel_kadrov";
        private string kppTableName = "Otdel_kpp";
        private string otchetTableName = "Otdel_otchetov";
        void UpdateKadrDGV()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var state = connection.State;

            var request = @"SELECT * FROM [" + kadrTableName + "]WHERE 1=1"; //WHERE surname + ' ' + name +' '+ patronymic LIKE '%" + search_fio.Text + "%'";//добавить фильтр по фио и номеру телефона          

            if (search_fio.Text != "")
            {
                request += @" AND [" + kadrTableName + "].surname+' '+[" + kadrTableName + "].patronymic+' '+[" + kadrTableName + "].name  LIKE+'%" + search_fio.Text + "%'";
            }
            if (search_twork.Text != "")
            {
                request += @" AND [" + kadrTableName + "].date_work LIKE '%" + search_twork.Text + "%'";
            }
            if (search_tabln.Text != "")
            {
                request += @" AND [" + kadrTableName + "].tabel_number LIKE '%" + search_tabln.Text + "%'";
            }
            if (search_nbrig.Text != "")
            {
                request += @" AND [" + kadrTableName + "].id_brigada LIKE '%" + search_nbrig.Text + "%'";
            }
            if (search_dolzh.Text != "")
            {
                request += @" AND [" + kadrTableName + "].position LIKE '%" + search_dolzh.Text + "%'";
            }


            var adapter = new SqlDataAdapter(request, connectionstring);
            var kadriTable = new DataTable();
            adapter.Fill(kadriTable);
            tabl_kadri.DataSource = kadriTable;
            tabl_kadri.Columns["id"].Visible = false;
            tabl_kadri.Columns["name"].HeaderText = "Имя";
            tabl_kadri.Columns["patronymic"].HeaderText = "Отчество";
            tabl_kadri.Columns["surname"].HeaderText = "Фамилия";
            tabl_kadri.Columns["tabel_number"].HeaderText = "Табельный номер";
            tabl_kadri.Columns["date_work"].HeaderText = "Дата приема на работу";
            tabl_kadri.Columns["id_brigada"].HeaderText = "Номер бригады";
            tabl_kadri.Columns["position"].HeaderText = "Должность";
        }
        void UpdateKppDGV()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var state = connection.State;

            var request = "SELECT * FROM [" + kppTableName + "] WHERE 1=1";//JOIN [" + kadrTableName + "] ON Otdel_kpp.id_employeer=Otdel_kadrov.Id WHERE 1=1"; //WHERE surname + ' ' + name +' '+ patronymic LIKE '%" + search_fio.Text + "%'";//добавить фильтр по фио и номеру телефона
           
            if (filter_emp.Text != "")
            {
                request += @" AND [" + kppTableName + "].id_employeer LIKE '%" + filter_emp.Text + "%'";
            }
            if (filter_time.Text != "")
            {
                request += @" AND [" + kppTableName + "].time_pass LIKE '%" + filter_time.Text + "%'";
            }
            if (filter_cheh.Text != "")
            {
                request += @" AND [" + kppTableName + "].tsekh LIKE '%" + filter_cheh.Text + "%'";
            }
            if (filter_napr.Text != "")
            {
                request += @" AND [" + kppTableName + "].way LIKE '%" + filter_napr.Text + "%'";
            }


            var adapter = new SqlDataAdapter(request, connectionstring);
            var kppTable = new DataTable();
            adapter.Fill(kppTable);
            tabl_kpp.DataSource = kppTable;
            tabl_kpp.Columns["id"].Visible = false;
            tabl_kpp.Columns["id_employeer"].HeaderText = "ID работника";
            tabl_kpp.Columns["time_pass"].HeaderText = "Время прохода";
            tabl_kpp.Columns["tsekh"].HeaderText = "Цех";
            tabl_kpp.Columns["way"].HeaderText = "Совершен вход или выход";
          /*  tabl_kpp.Columns["id1"].Visible = false;
            tabl_kpp.Columns["name"].Visible = false;
            tabl_kpp.Columns["surname"].Visible = false;
            tabl_kpp.Columns["patronymic"].Visible = false;
            tabl_kpp.Columns["tabel_number"].HeaderText = "Табельный номер";
            tabl_kpp.Columns["date_work"].Visible = false;
            tabl_kpp.Columns["id_brigada"].Visible = false;
            tabl_kpp.Columns["position"].Visible = false;*/




        }
        void UpdateOtchetDGV()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var state = connection.State;
            // var request = @"SELECT * FROM Chugrina_abonent RIGHT JOIN Chugrina_abonent_hos_cont ON Chugrina_abonent.Id=Chugrina_abonent_hos_cont.abonent_id LEFT JOIN Chugrina_contact ON Chugrina_contact.Id=Chugrina_abonent_hos_cont.contact_id LEFT JOIN Chugrina_provider ON Chugrina_contact.provider_id=Chugrina_provider.Id WHERE 1=1";
            var request = @"SELECT * FROM [" + kadrTableName + "]  JOIN [" + kppTableName + "] ON [" + kadrTableName + "].tabel_number=[" + kppTableName + "].id_employeer WHERE 1=1";
           /* string sql = "SELECT COUNT(*) FROM Otdel_kpp WHERE way='Vhod'";
            using (SqlConnection conn = new SqlConnection(ConnectionString))*/
                
            {
               

                if (search_ot_cheh.Text != "")
                {
                    request += @" AND [" + kppTableName + "].tsekh LIKE '" + search_ot_cheh.Text + "'";
                }
                if (search_ot_brig.Text != "")
                {
                    request += @" AND [" + kadrTableName + "].id_brigada LIKE '" + search_ot_brig.Text + "'";
                }

                if (search_ot_dolzh.Text != "")
                {
                    request += @" AND [" + kadrTableName + "].position LIKE '%" + search_ot_dolzh.Text + "%'";
                }

                if ((n_obj_tb.Text != "") && (time1_tb.Text != "") && (time2_tb.Text != ""))
                {
                    int n;
                    request += @" AND [" + kppTableName + "].tsekh LIKE '%" + n_obj_tb.Text + "%'";
                    request += @" AND [" + kppTableName + "].time_pass BETWEEN '" + time1_tb.Text + "' AND '" + time2_tb.Text + "'";
                    //string sql 
                    request += "SELECT COUNT(*) FROM [" + kppTableName + "] WHERE [" + kppTableName + "].tsekh LIKE '%" + n_obj_tb.Text + "%' AND [" + kppTableName + "].time_pass BETWEEN '" + time1_tb.Text + "' AND '" + time2_tb.Text + "'";
                    using (SqlConnection conn = new SqlConnection(ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand(request, conn); conn.Open();
                        n = Convert.ToInt32(cmd.ExecuteScalar());
                        otc_count_em.Text = n.ToString();
                       
                    }
                }

                if (n_brig_otc.Text != "")
                {
                    request += @" AND [" + kadrTableName + "].id_brigada LIKE '%" + n_brig_otc.Text + "%'";

                    var vh_time = "SELECT time_pass FROM  [" + kadrTableName + "] JOIN [" + kppTableName + "] ON  [" + kadrTableName + "].tabel_number=[" + kppTableName + "].id_employeer WHERE way='Vhod' ORDER BY surname";
                    var vih_time = "SELECT  time_pass FROM  [" + kadrTableName + "] JOIN [" + kppTableName + "] ON  [" + kadrTableName + "].tabel_number=[" + kppTableName + "].id_employeer WHERE way='Vihod' ORDER BY surname";
                     var vhodAdapter = new SqlDataAdapter(vh_time, ConnectionString);
                     var vihodAdapter = new SqlDataAdapter(vih_time, ConnectionString);
                    var vhTbl = new DataTable(); var vihTbl = new DataTable(); 
                    vhodAdapter.Fill(vhTbl); vihodAdapter.Fill(vihTbl);
                    string[] vhod = new string[vhTbl.Rows.Count]; string[] vihod = new string[vihTbl.Rows.Count];  
                    int i=0,j=0;
                     foreach (DataRow row in vhTbl.Rows)
                     {
                         vhod[i]=row["time_pass"].ToString();
                         i++;
                     }
                    foreach (DataRow row in vihTbl.Rows)
                     {
                         vihod[j] =row["time_pass"].ToString();
                         j++;
                     }
                    int Minute_w=0;
                    for (int k = 0; k < vhTbl.Rows.Count; k++)
                    {
                       string req;
                       req = " SELECT DATEDIFF ( MINUTE,'" + Convert.ToDateTime(vhod[k]).ToString("HH:mm:ss") + "' , '" + Convert.ToDateTime(vihod[k]).ToString("HH:mm:ss") + "')";
                        using (SqlConnection conn = new SqlConnection(ConnectionString))
                        {
                            SqlCommand cmd = new SqlCommand(request, conn); conn.Open();
                            Minute_w += Convert.ToInt32(cmd.ExecuteScalar());
                            conn.Close();
                        }

                    }
                    if (Minute_w % 60>9)
                    time_work_brig.Text = "" + Minute_w / 60 + ":" + Minute_w % 60 + "";
                    if (Minute_w % 60<9)
                        time_work_brig.Text = "" + Minute_w / 60 + ":0" + Minute_w % 60 + "";
                   
                }
                if (obj_otch_tb.Text != "")
                {
                    request += @" AND [" + kppTableName + "].tsekh LIKE " + obj_otch_tb.Text+"ORDER BY surname";
                    var vh_time = "SELECT time_pass FROM  [" + kadrTableName + "] JOIN [" + kppTableName + "] ON  [" + kadrTableName + "].tabel_number=[" + kppTableName + "].id_employeer WHERE tsekh LIKE " + obj_otch_tb.Text+" AND way='Vhod' ORDER BY surname";
                    var vih_time = "SELECT  time_pass FROM  [" + kadrTableName + "] JOIN [" + kppTableName + "] ON  [" + kadrTableName + "].tabel_number=[" + kppTableName + "].id_employeer WHERE  tsekh LIKE " + obj_otch_tb.Text+"AND way='Vihod' ORDER BY surname";
                    var vhodAdapter = new SqlDataAdapter(vh_time, ConnectionString);
                    var vihodAdapter = new SqlDataAdapter(vih_time, ConnectionString);
                    var vhTbl = new DataTable(); var vihTbl = new DataTable();
                    vhodAdapter.Fill(vhTbl); vihodAdapter.Fill(vihTbl);
                    string[] vhod = new string[vhTbl.Rows.Count]; string[] vihod = new string[vihTbl.Rows.Count];
                    int i = 0, j = 0;
                    foreach (DataRow row in vhTbl.Rows)
                    {
                        vhod[i] = row["time_pass"].ToString();
                        i++;
                    }
                    foreach (DataRow row in vihTbl.Rows)
                    {
                        vihod[j] = row["time_pass"].ToString();
                        j++;
                    }
                    int Minute_w = 0;
                    for (int k = 0; k < vhod.Length; k++)
                    {
                        string req;
                        req = "  SELECT DATEDIFF ( MINUTE,'" + Convert.ToDateTime(vhod[k]).ToString("HH:mm:ss.fff") + "' , '" + Convert.ToDateTime(vihod[k]).ToString("HH:mm:ss.fff") + "')";  
                        using (SqlConnection conn = new SqlConnection(ConnectionString))
                        {
                            SqlCommand cmd = new SqlCommand(req, conn);  conn.Open();
                            Minute_w += Convert.ToInt32(cmd.ExecuteScalar());
                            conn.Close();
                        }

                    }
                    if (Minute_w % 60 > 9)
                        time_work_obj.Text = "" + Minute_w / 60 + ":" + Minute_w % 60 + "";
                    if (Minute_w % 60 < 9)
                        time_work_obj.Text = "" + Minute_w / 60 + ":0" + Minute_w % 60 + "";
                    
                }
                

                var adapter = new SqlDataAdapter(request, connectionstring);
                var otchetTable = new DataTable();
                adapter.Fill(otchetTable);
                tabl_otchet.DataSource = otchetTable;
                tabl_otchet.Columns["id"].Visible = false;
                tabl_otchet.Columns["name"].HeaderText = "Имя";
                tabl_otchet.Columns["patronymic"].HeaderText = "Отчество";
                tabl_otchet.Columns["surname"].HeaderText = "Фамилия";
                tabl_otchet.Columns["tabel_number"].HeaderText = "Табельный номер";
                tabl_otchet.Columns["date_work"].Visible = false;
                tabl_otchet.Columns["id_brigada"].HeaderText = "Номер бригады";
                tabl_otchet.Columns["position"].HeaderText = "Должность";
                tabl_otchet.Columns["id1"].Visible = false;
                tabl_otchet.Columns["id_employeer"].Visible = false;
                tabl_otchet.Columns["time_pass"].HeaderText = "Время прохода";
                tabl_otchet.Columns["tsekh"].HeaderText = "Объект";
                tabl_otchet.Columns["way"].HeaderText = "Совершен вход или выход";

            }
        }

     
        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateKadrDGV();
            UpdateKppDGV();
            UpdateOtchetDGV();
        }

       

        private void edit_button_Click(object sender, EventArgs e)
        {
            var row = tabl_kadri.SelectedRows.Count > 0 ? tabl_kadri.SelectedRows[0] : null;
            if(row==null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new AbForm();
            form.surname_tb.Text= row.Cells["surname"].Value.ToString();
            form.name_tb.Text = row.Cells["name"].Value.ToString();
            form.patronymic_tb.Text = row.Cells["patronymic"].Value.ToString();
            form.tab_num_tb.Text = row.Cells["address"].Value.ToString();
            form.dolzhnost_tb.Text = row.Cells["comment"].Value.ToString();
            form.date_work_tb.Text = row.Cells["birthday"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.surname_tb.Text;
                var name = form.name_tb.Text;
                var patronymic = form.patronymic_tb.Text;
                var comment = form.dolzhnost_tb.Text;
                var address = form.tab_num_tb.Text;
                var birthday = form.date_work_tb.Text;
                var id = row.Cells["Id"].Value.ToString();

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE таблица SET surname = '" + surname + "', name = '" + name + "', " +
                    "patronymic = '" + patronymic + "', address = '" + address + "', comment = '" + comment + "', birthday = '" + birthday + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
              
            }
        }

        private void delete_button_Click(object sender, EventArgs e)
        {
            var row = tabl_kadri.SelectedRows.Count > 0 ? tabl_kadri.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Lapshin_abonent WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            
        }

        
        private void add_emp_Click(object sender, EventArgs e)
        {
             var form = new AbForm();
            {
                var getReq = "SELECT *FROM [" + otchetTableName + "]";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var brigadaTbl = new DataTable();
                contactAdapter.Fill(brigadaTbl);

                foreach (DataRow row in brigadaTbl.Rows)
                {
                    dict.Add((int)row["Id"], row["brigada_id"].ToString());
                }
                form.ProviderData = dict;
            }

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.name_tb.Text;
                var surname = form.surname_tb.Text;
                var patronymic = form.patronymic_tb.Text;
                var dolzhnost = form.dolzhnost_tb.Text;
                var tab_num = form.tab_num_tb.Text;
                var date_work = form.date_work_tb.Text;
                var brigada = form.BrigadaID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO [" + kadrTableName + "] (name, surname, patronymic, tabel_number, position, date_work, id_brigada) " +
                    "VALUES ('" + name + "', '" + surname + "', '" + patronymic + "', '" + tab_num + "', '" + dolzhnost + "', '" + date_work + "','" + brigada.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                UpdateKadrDGV();
            }
        }

        private void edit_emp_Click(object sender, EventArgs e)
        {
            var row = tabl_kadri.SelectedRows.Count > 0 ? tabl_kadri.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new AbForm();
            form.surname_tb.Text = row.Cells["surname"].Value.ToString();
            form.name_tb.Text = row.Cells["name"].Value.ToString();
            form.patronymic_tb.Text = row.Cells["patronymic"].Value.ToString();
            form.tab_num_tb.Text = row.Cells["tabel_number"].Value.ToString();
            form.dolzhnost_tb.Text = row.Cells["position"].Value.ToString();
            form.date_work_tb.Text = row.Cells["date_work"].Value.ToString();
            {
                var getReq = "SELECT *FROM Otdel_otchetov";
                var brigAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                brigAdapter.Fill(providerTbl);

                foreach (DataRow dbrow in providerTbl.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["brigada_id"].ToString());
                }
                form.ProviderData = dict;
            }
            form.BrigadaID = (int)row.Cells["id_brigada"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.surname_tb.Text;
                var name = form.name_tb.Text;
                var patronymic = form.patronymic_tb.Text;
                var dolzhnost = form.dolzhnost_tb.Text;
                var tab_num = form.tab_num_tb.Text;
                var date_work = Convert.ToDateTime(form.date_work_tb.Text).ToString("yyyy-MM-dd HH:mm:ss.fff"); ;
                var brigada = form.BrigadaID;
                var id = row.Cells["Id"].Value.ToString();

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE [" + kadrTableName + "] SET surname = '" + surname + "', name = '" + name + "', patronymic='" + patronymic + "',  tabel_number = '" + tab_num + "', date_work = '" + date_work + "',  position = '" + dolzhnost + "', id_brigada='" + brigada.ToString() + "'WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                UpdateKadrDGV();
            }
        }

        private void delete_emp_Click(object sender, EventArgs e)
        {
            var row = tabl_kadri.SelectedRows.Count > 0 ? tabl_kadri.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM [" + kadrTableName + "] WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            UpdateKadrDGV();
        }

        private void add_kpp_Click(object sender, EventArgs e)
        {
            var form = new CnForm();
            {
                var request = "SELECT * FROM [" + otchetTableName + "]";
                var adapter = new SqlDataAdapter(request, ConnectionString);
                var providerTable = new DataTable();
                adapter.Fill(providerTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in providerTable.Rows)
                {
                    dict.Add((int)row["Id"], row["cheh_id"].ToString());
                }
                form.ProviderData = dict;
            }
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var emp = form.emp_tb.Text;
                var time = form.time_tb.Text;
                var napr = form.napr_tb.Text;
                var cheh_id = form.ChehId;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO [" + kppTableName + "] (id_employeer, time_pass, tsekh, way) VALUES ('" + emp + "', '" + time + "', '" + cheh_id.ToString() + "', '" + napr +"')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                UpdateKppDGV();
            }
        }

   
        private void edit_kpp_Click_1(object sender, EventArgs e)
        {
            var row = tabl_kpp.SelectedRows.Count > 0 ? tabl_kpp.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new CnForm();
            form.emp_tb.Text = row.Cells["id_employeer"].Value.ToString();
            form.time_tb.Text = row.Cells["time_pass"].Value.ToString();
            form.napr_tb.Text = row.Cells["way"].Value.ToString();
            {
                var request = "SELECT * FROM [" + otchetTableName + "]";
                var adapter = new SqlDataAdapter(request, ConnectionString);
                var providerTable = new DataTable();
                adapter.Fill(providerTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbRow in providerTable.Rows)
                {
                    dict.Add((int)dbRow["Id"], dbRow["cheh_id"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ChehId = (int)row.Cells["tsekh"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var emp= form.emp_tb.Text;
                var time = form.time_tb.Text;
                var napr = form.napr_tb.Text;
                var id = row.Cells["Id"].Value.ToString();
                var cheh_id = form.ChehId;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE [" + kppTableName + "]  SET id_employeer  = '" + emp + "', time_pass = '" + time + "', " +
                    "tsekh = '" + cheh_id.ToString() + "', way = '" + napr + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); 
                connection.Close();
                UpdateKppDGV();
            }
        }
        private void delete_kpp_Click(object sender, EventArgs e)
        {
            var row = tabl_kpp.SelectedRows.Count > 0 ? tabl_kpp.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM[" + kppTableName + "]  WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            UpdateKppDGV();
        }

        private void search_fio_TextChanged(object sender, EventArgs e)
        {
            UpdateKadrDGV();
        }

        private void search_twork_TextChanged(object sender, EventArgs e)
        {
            UpdateKadrDGV();
        }

        private void search_tabln_TextChanged(object sender, EventArgs e)
        {
            UpdateKadrDGV();
        }

        private void search_nbrig_TextChanged(object sender, EventArgs e)
        {
            UpdateKadrDGV();
        }

        private void search_dolzh_TextChanged(object sender, EventArgs e)
        {
            UpdateKadrDGV();
        }

        private void filter_emp_TextChanged(object sender, EventArgs e)
        {
            UpdateKppDGV();
        }

        private void filter_time_TextChanged(object sender, EventArgs e)
        {
            UpdateKppDGV();
        }

        private void filter_cheh_TextChanged(object sender, EventArgs e)
        {
            UpdateKppDGV();
        }

        private void filter_napr_TextChanged(object sender, EventArgs e)
        {
            UpdateKppDGV();
        }

        private void search_ot_cheh_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void search_ot_brig_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void search_ot_dolzh_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void n_obj_tb_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void time1_tb_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void time2_tb_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void n_brig_otc_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void time_work_brig_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void obj_otch_tb_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

        private void time_work_obj_TextChanged(object sender, EventArgs e)
        {
            UpdateOtchetDGV();
        }

       

      

        
        

        
    }
}
