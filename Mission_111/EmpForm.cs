﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mission_1
{
    public partial class AbForm : Form
    {
        public AbForm()
        {
            InitializeComponent();
        }

        private void ok_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        public Dictionary<int, string> ProviderData
        {
            set
            {
                EmpForm_brig.DataSource = value.ToArray();
                EmpForm_brig.DisplayMember = "Value";
            }
        }
        public int BrigadaID
        {
            get { return ((KeyValuePair<int, string>)EmpForm_brig.SelectedItem).Key; }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in EmpForm_brig.Items)
                {
                    if (item.Key == value) break;
                    idx++;
                }
                EmpForm_brig.SelectedIndex = idx;
            }
        }
    }
}
