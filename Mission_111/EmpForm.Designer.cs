﻿namespace Mission_1
{
    partial class AbForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.surname_tb = new System.Windows.Forms.TextBox();
            this.name_tb = new System.Windows.Forms.TextBox();
            this.patronymic_tb = new System.Windows.Forms.TextBox();
            this.tab_num_tb = new System.Windows.Forms.TextBox();
            this.dolzhnost_tb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ok_button = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.date_work_tb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.EmpForm_brig = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // surname_tb
            // 
            this.surname_tb.Location = new System.Drawing.Point(23, 12);
            this.surname_tb.Name = "surname_tb";
            this.surname_tb.Size = new System.Drawing.Size(100, 20);
            this.surname_tb.TabIndex = 0;
            // 
            // name_tb
            // 
            this.name_tb.Location = new System.Drawing.Point(23, 45);
            this.name_tb.Name = "name_tb";
            this.name_tb.Size = new System.Drawing.Size(100, 20);
            this.name_tb.TabIndex = 0;
            // 
            // patronymic_tb
            // 
            this.patronymic_tb.Location = new System.Drawing.Point(23, 71);
            this.patronymic_tb.Name = "patronymic_tb";
            this.patronymic_tb.Size = new System.Drawing.Size(100, 20);
            this.patronymic_tb.TabIndex = 0;
            // 
            // tab_num_tb
            // 
            this.tab_num_tb.Location = new System.Drawing.Point(23, 123);
            this.tab_num_tb.Name = "tab_num_tb";
            this.tab_num_tb.Size = new System.Drawing.Size(100, 20);
            this.tab_num_tb.TabIndex = 0;
            // 
            // dolzhnost_tb
            // 
            this.dolzhnost_tb.Location = new System.Drawing.Point(23, 97);
            this.dolzhnost_tb.Name = "dolzhnost_tb";
            this.dolzhnost_tb.Size = new System.Drawing.Size(100, 20);
            this.dolzhnost_tb.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(188, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(188, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(152, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Табельный номер";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(177, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Должность";
            // 
            // ok_button
            // 
            this.ok_button.Location = new System.Drawing.Point(37, 226);
            this.ok_button.Name = "ok_button";
            this.ok_button.Size = new System.Drawing.Size(75, 23);
            this.ok_button.TabIndex = 2;
            this.ok_button.Text = "OK";
            this.ok_button.UseVisualStyleBackColor = true;
            this.ok_button.Click += new System.EventHandler(this.ok_button_Click);
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(167, 226);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(75, 23);
            this.cancel_button.TabIndex = 2;
            this.cancel_button.Text = "Отмена";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // date_work_tb
            // 
            this.date_work_tb.Location = new System.Drawing.Point(23, 149);
            this.date_work_tb.Name = "date_work_tb";
            this.date_work_tb.Size = new System.Drawing.Size(100, 20);
            this.date_work_tb.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Дата приёма";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(164, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Номер бригады";
            // 
            // EmpForm_brig
            // 
            this.EmpForm_brig.FormattingEnabled = true;
            this.EmpForm_brig.Location = new System.Drawing.Point(12, 185);
            this.EmpForm_brig.Name = "EmpForm_brig";
            this.EmpForm_brig.Size = new System.Drawing.Size(111, 21);
            this.EmpForm_brig.TabIndex = 6;
            // 
            // AbForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 271);
            this.Controls.Add(this.EmpForm_brig);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.date_work_tb);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.ok_button);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dolzhnost_tb);
            this.Controls.Add(this.tab_num_tb);
            this.Controls.Add(this.patronymic_tb);
            this.Controls.Add(this.name_tb);
            this.Controls.Add(this.surname_tb);
            this.Name = "AbForm";
            this.Text = "Сотрудник";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ok_button;
        private System.Windows.Forms.Button cancel_button;
        public System.Windows.Forms.TextBox surname_tb;
        public System.Windows.Forms.TextBox name_tb;
        public System.Windows.Forms.TextBox patronymic_tb;
        public System.Windows.Forms.TextBox tab_num_tb;
        public System.Windows.Forms.TextBox dolzhnost_tb;
        public System.Windows.Forms.TextBox date_work_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox EmpForm_brig;
    }
}