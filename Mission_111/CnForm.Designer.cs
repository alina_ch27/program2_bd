﻿namespace Mission_1
{
    partial class CnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ok_button = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.emp_tb = new System.Windows.Forms.TextBox();
            this.time_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.brig_cb = new System.Windows.Forms.ComboBox();
            this.napr_tb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ok_button
            // 
            this.ok_button.Location = new System.Drawing.Point(37, 164);
            this.ok_button.Name = "ok_button";
            this.ok_button.Size = new System.Drawing.Size(75, 23);
            this.ok_button.TabIndex = 0;
            this.ok_button.Text = "OK";
            this.ok_button.UseVisualStyleBackColor = true;
            this.ok_button.Click += new System.EventHandler(this.ok_button_Click);
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(153, 164);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(75, 23);
            this.cancel_button.TabIndex = 0;
            this.cancel_button.Text = "Отмена";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сотрудник";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Время прохода";
            // 
            // emp_tb
            // 
            this.emp_tb.Location = new System.Drawing.Point(12, 12);
            this.emp_tb.Name = "emp_tb";
            this.emp_tb.Size = new System.Drawing.Size(114, 20);
            this.emp_tb.TabIndex = 3;
            // 
            // time_tb
            // 
            this.time_tb.Location = new System.Drawing.Point(12, 42);
            this.time_tb.Name = "time_tb";
            this.time_tb.Size = new System.Drawing.Size(114, 20);
            this.time_tb.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Цех";
            // 
            // brig_cb
            // 
            this.brig_cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.brig_cb.FormattingEnabled = true;
            this.brig_cb.Location = new System.Drawing.Point(12, 120);
            this.brig_cb.Name = "brig_cb";
            this.brig_cb.Size = new System.Drawing.Size(114, 21);
            this.brig_cb.TabIndex = 6;
            // 
            // napr_tb
            // 
            this.napr_tb.Location = new System.Drawing.Point(12, 77);
            this.napr_tb.Name = "napr_tb";
            this.napr_tb.Size = new System.Drawing.Size(114, 20);
            this.napr_tb.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(189, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Вход/выход";
            // 
            // CnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 207);
            this.Controls.Add(this.napr_tb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.brig_cb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.time_tb);
            this.Controls.Add(this.emp_tb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.ok_button);
            this.Name = "CnForm";
            this.Text = "КПП";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ok_button;
        private System.Windows.Forms.Button cancel_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox emp_tb;
        public System.Windows.Forms.TextBox time_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox brig_cb;
        public System.Windows.Forms.TextBox napr_tb;
        private System.Windows.Forms.Label label4;
    }
}