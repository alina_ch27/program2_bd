﻿namespace Mission_1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.otchet_page = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.obj_otch_tb = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.time_work_obj = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.n_brig_otc = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.time_work_brig = new System.Windows.Forms.TextBox();
            this.часов = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.time1_tb = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.time2_tb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.n_obj_tb = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.otc_count_em = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.search_ot_brig = new System.Windows.Forms.TextBox();
            this.search_ot_cheh = new System.Windows.Forms.TextBox();
            this.Объект = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.search_ot_dolzh = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabl_otchet = new System.Windows.Forms.DataGridView();
            this.kpp_page = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.filter_napr = new System.Windows.Forms.TextBox();
            this.filter_cheh = new System.Windows.Forms.TextBox();
            this.filter_time = new System.Windows.Forms.TextBox();
            this.filter_emp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.delete_kpp = new System.Windows.Forms.Button();
            this.edit_kpp = new System.Windows.Forms.Button();
            this.add_kpp = new System.Windows.Forms.Button();
            this.tabl_kpp = new System.Windows.Forms.DataGridView();
            this.kadri_page = new System.Windows.Forms.TabPage();
            this.search_twork = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.search_tabln = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.search_nbrig = new System.Windows.Forms.TextBox();
            this.search_dolzh = new System.Windows.Forms.TextBox();
            this.search_fio = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.delete_emp = new System.Windows.Forms.Button();
            this.edit_emp = new System.Windows.Forms.Button();
            this.add_emp = new System.Windows.Forms.Button();
            this.tabl_kadri = new System.Windows.Forms.DataGridView();
            this.Main_tabControl = new System.Windows.Forms.TabControl();
            this.otchet_page.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_otchet)).BeginInit();
            this.kpp_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kpp)).BeginInit();
            this.kadri_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kadri)).BeginInit();
            this.Main_tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            // 
            // otchet_page
            // 
            this.otchet_page.Controls.Add(this.groupBox4);
            this.otchet_page.Controls.Add(this.groupBox3);
            this.otchet_page.Controls.Add(this.groupBox2);
            this.otchet_page.Controls.Add(this.groupBox1);
            this.otchet_page.Controls.Add(this.richTextBox1);
            this.otchet_page.Controls.Add(this.tabl_otchet);
            this.otchet_page.Location = new System.Drawing.Point(4, 22);
            this.otchet_page.Name = "otchet_page";
            this.otchet_page.Size = new System.Drawing.Size(1157, 590);
            this.otchet_page.TabIndex = 2;
            this.otchet_page.Text = "Отдел отчетов";
            this.otchet_page.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.obj_otch_tb);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.time_work_obj);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Location = new System.Drawing.Point(376, 530);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(406, 57);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Часы-объект";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(66, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "На объекте";
            // 
            // obj_otch_tb
            // 
            this.obj_otch_tb.Location = new System.Drawing.Point(78, 21);
            this.obj_otch_tb.Name = "obj_otch_tb";
            this.obj_otch_tb.Size = new System.Drawing.Size(85, 20);
            this.obj_otch_tb.TabIndex = 22;
            this.obj_otch_tb.TextChanged += new System.EventHandler(this.obj_otch_tb_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(169, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "отработано";
            // 
            // time_work_obj
            // 
            this.time_work_obj.Location = new System.Drawing.Point(246, 21);
            this.time_work_obj.Name = "time_work_obj";
            this.time_work_obj.Size = new System.Drawing.Size(100, 20);
            this.time_work_obj.TabIndex = 24;
            this.time_work_obj.TextChanged += new System.EventHandler(this.time_work_obj_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(352, 28);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "часов";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.n_brig_otc);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.time_work_brig);
            this.groupBox3.Controls.Add(this.часов);
            this.groupBox3.Location = new System.Drawing.Point(22, 525);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(329, 57);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Часы-бригада";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Бригада";
            // 
            // n_brig_otc
            // 
            this.n_brig_otc.Location = new System.Drawing.Point(58, 26);
            this.n_brig_otc.Name = "n_brig_otc";
            this.n_brig_otc.Size = new System.Drawing.Size(65, 20);
            this.n_brig_otc.TabIndex = 16;
            this.n_brig_otc.TextChanged += new System.EventHandler(this.n_brig_otc_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(129, 29);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "отработала";
            // 
            // time_work_brig
            // 
            this.time_work_brig.Location = new System.Drawing.Point(200, 26);
            this.time_work_brig.Name = "time_work_brig";
            this.time_work_brig.Size = new System.Drawing.Size(81, 20);
            this.time_work_brig.TabIndex = 19;
            this.time_work_brig.TextChanged += new System.EventHandler(this.time_work_brig_TextChanged);
            // 
            // часов
            // 
            this.часов.AutoSize = true;
            this.часов.Location = new System.Drawing.Point(287, 29);
            this.часов.Name = "часов";
            this.часов.Size = new System.Drawing.Size(36, 13);
            this.часов.TabIndex = 20;
            this.часов.Text = "часов";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.time1_tb);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.time2_tb);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.n_obj_tb);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.otc_count_em);
            this.groupBox2.Location = new System.Drawing.Point(376, 447);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(469, 72);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Люди-объект";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "С";
            // 
            // time1_tb
            // 
            this.time1_tb.Location = new System.Drawing.Point(26, 33);
            this.time1_tb.Name = "time1_tb";
            this.time1_tb.Size = new System.Drawing.Size(60, 20);
            this.time1_tb.TabIndex = 8;
            this.time1_tb.TextChanged += new System.EventHandler(this.time1_tb_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(92, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "до";
            // 
            // time2_tb
            // 
            this.time2_tb.Location = new System.Drawing.Point(117, 33);
            this.time2_tb.Name = "time2_tb";
            this.time2_tb.Size = new System.Drawing.Size(59, 20);
            this.time2_tb.TabIndex = 9;
            this.time2_tb.TextChanged += new System.EventHandler(this.time2_tb_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(175, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "на объекте ";
            // 
            // n_obj_tb
            // 
            this.n_obj_tb.Location = new System.Drawing.Point(248, 33);
            this.n_obj_tb.Name = "n_obj_tb";
            this.n_obj_tb.Size = new System.Drawing.Size(41, 20);
            this.n_obj_tb.TabIndex = 14;
            this.n_obj_tb.TextChanged += new System.EventHandler(this.n_obj_tb_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(295, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "сколько человек";
            // 
            // otc_count_em
            // 
            this.otc_count_em.Location = new System.Drawing.Point(394, 36);
            this.otc_count_em.Name = "otc_count_em";
            this.otc_count_em.Size = new System.Drawing.Size(56, 20);
            this.otc_count_em.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.search_ot_brig);
            this.groupBox1.Controls.Add(this.search_ot_cheh);
            this.groupBox1.Controls.Add(this.Объект);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.search_ot_dolzh);
            this.groupBox1.Location = new System.Drawing.Point(22, 447);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 72);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск";
            // 
            // search_ot_brig
            // 
            this.search_ot_brig.Location = new System.Drawing.Point(112, 40);
            this.search_ot_brig.Name = "search_ot_brig";
            this.search_ot_brig.Size = new System.Drawing.Size(100, 20);
            this.search_ot_brig.TabIndex = 2;
            this.search_ot_brig.TextChanged += new System.EventHandler(this.search_ot_brig_TextChanged);
            // 
            // search_ot_cheh
            // 
            this.search_ot_cheh.Location = new System.Drawing.Point(6, 40);
            this.search_ot_cheh.Name = "search_ot_cheh";
            this.search_ot_cheh.Size = new System.Drawing.Size(100, 20);
            this.search_ot_cheh.TabIndex = 1;
            this.search_ot_cheh.TextChanged += new System.EventHandler(this.search_ot_cheh_TextChanged);
            // 
            // Объект
            // 
            this.Объект.AutoSize = true;
            this.Объект.Location = new System.Drawing.Point(28, 24);
            this.Объект.Name = "Объект";
            this.Объект.Size = new System.Drawing.Size(45, 13);
            this.Объект.TabIndex = 4;
            this.Объект.Text = "Объект";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(128, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Бригада";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(220, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Специальность";
            // 
            // search_ot_dolzh
            // 
            this.search_ot_dolzh.Location = new System.Drawing.Point(218, 40);
            this.search_ot_dolzh.Name = "search_ot_dolzh";
            this.search_ot_dolzh.Size = new System.Drawing.Size(100, 20);
            this.search_ot_dolzh.TabIndex = 3;
            this.search_ot_dolzh.TextChanged += new System.EventHandler(this.search_ot_dolzh_TextChanged);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(995, 17);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(154, 550);
            this.richTextBox1.TabIndex = 26;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // tabl_otchet
            // 
            this.tabl_otchet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_otchet.Location = new System.Drawing.Point(3, 3);
            this.tabl_otchet.MultiSelect = false;
            this.tabl_otchet.Name = "tabl_otchet";
            this.tabl_otchet.RowHeadersWidth = 62;
            this.tabl_otchet.Size = new System.Drawing.Size(986, 438);
            this.tabl_otchet.TabIndex = 0;
            // 
            // kpp_page
            // 
            this.kpp_page.Controls.Add(this.label5);
            this.kpp_page.Controls.Add(this.label6);
            this.kpp_page.Controls.Add(this.filter_napr);
            this.kpp_page.Controls.Add(this.filter_cheh);
            this.kpp_page.Controls.Add(this.filter_time);
            this.kpp_page.Controls.Add(this.filter_emp);
            this.kpp_page.Controls.Add(this.label1);
            this.kpp_page.Controls.Add(this.label2);
            this.kpp_page.Controls.Add(this.label4);
            this.kpp_page.Controls.Add(this.delete_kpp);
            this.kpp_page.Controls.Add(this.edit_kpp);
            this.kpp_page.Controls.Add(this.add_kpp);
            this.kpp_page.Controls.Add(this.tabl_kpp);
            this.kpp_page.Location = new System.Drawing.Point(4, 22);
            this.kpp_page.Name = "kpp_page";
            this.kpp_page.Padding = new System.Windows.Forms.Padding(3);
            this.kpp_page.Size = new System.Drawing.Size(1157, 590);
            this.kpp_page.TabIndex = 1;
            this.kpp_page.Text = "Отдел КПП";
            this.kpp_page.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(767, 491);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Вход/выход";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(663, 491);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Цех";
            // 
            // filter_napr
            // 
            this.filter_napr.Location = new System.Drawing.Point(770, 520);
            this.filter_napr.Name = "filter_napr";
            this.filter_napr.Size = new System.Drawing.Size(99, 20);
            this.filter_napr.TabIndex = 10;
            this.filter_napr.TextChanged += new System.EventHandler(this.filter_napr_TextChanged);
            // 
            // filter_cheh
            // 
            this.filter_cheh.Location = new System.Drawing.Point(627, 520);
            this.filter_cheh.Name = "filter_cheh";
            this.filter_cheh.Size = new System.Drawing.Size(93, 20);
            this.filter_cheh.TabIndex = 11;
            this.filter_cheh.TextChanged += new System.EventHandler(this.filter_cheh_TextChanged);
            // 
            // filter_time
            // 
            this.filter_time.Location = new System.Drawing.Point(474, 520);
            this.filter_time.Name = "filter_time";
            this.filter_time.Size = new System.Drawing.Size(99, 20);
            this.filter_time.TabIndex = 5;
            this.filter_time.TextChanged += new System.EventHandler(this.filter_time_TextChanged);
            // 
            // filter_emp
            // 
            this.filter_emp.Location = new System.Drawing.Point(339, 520);
            this.filter_emp.Name = "filter_emp";
            this.filter_emp.Size = new System.Drawing.Size(93, 20);
            this.filter_emp.TabIndex = 6;
            this.filter_emp.TextChanged += new System.EventHandler(this.filter_emp_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(580, 469);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Поиск";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(506, 491);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Время";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(355, 491);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Сотрудник";
            // 
            // delete_kpp
            // 
            this.delete_kpp.Location = new System.Drawing.Point(220, 515);
            this.delete_kpp.Name = "delete_kpp";
            this.delete_kpp.Size = new System.Drawing.Size(100, 25);
            this.delete_kpp.TabIndex = 4;
            this.delete_kpp.Text = "Удалить";
            this.delete_kpp.UseVisualStyleBackColor = true;
            this.delete_kpp.Click += new System.EventHandler(this.delete_kpp_Click);
            // 
            // edit_kpp
            // 
            this.edit_kpp.Location = new System.Drawing.Point(114, 515);
            this.edit_kpp.Name = "edit_kpp";
            this.edit_kpp.Size = new System.Drawing.Size(100, 25);
            this.edit_kpp.TabIndex = 3;
            this.edit_kpp.Text = "Редактировать";
            this.edit_kpp.UseVisualStyleBackColor = true;
            this.edit_kpp.Click += new System.EventHandler(this.edit_kpp_Click_1);
            // 
            // add_kpp
            // 
            this.add_kpp.Location = new System.Drawing.Point(0, 515);
            this.add_kpp.Name = "add_kpp";
            this.add_kpp.Size = new System.Drawing.Size(100, 25);
            this.add_kpp.TabIndex = 2;
            this.add_kpp.Text = "Регистрация";
            this.add_kpp.UseVisualStyleBackColor = true;
            this.add_kpp.Click += new System.EventHandler(this.add_kpp_Click);
            // 
            // tabl_kpp
            // 
            this.tabl_kpp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_kpp.Location = new System.Drawing.Point(3, 3);
            this.tabl_kpp.MultiSelect = false;
            this.tabl_kpp.Name = "tabl_kpp";
            this.tabl_kpp.RowHeadersWidth = 62;
            this.tabl_kpp.Size = new System.Drawing.Size(969, 452);
            this.tabl_kpp.TabIndex = 0;
            // 
            // kadri_page
            // 
            this.kadri_page.Controls.Add(this.search_twork);
            this.kadri_page.Controls.Add(this.label11);
            this.kadri_page.Controls.Add(this.search_tabln);
            this.kadri_page.Controls.Add(this.label10);
            this.kadri_page.Controls.Add(this.search_nbrig);
            this.kadri_page.Controls.Add(this.search_dolzh);
            this.kadri_page.Controls.Add(this.search_fio);
            this.kadri_page.Controls.Add(this.label9);
            this.kadri_page.Controls.Add(this.label8);
            this.kadri_page.Controls.Add(this.label7);
            this.kadri_page.Controls.Add(this.label3);
            this.kadri_page.Controls.Add(this.delete_emp);
            this.kadri_page.Controls.Add(this.edit_emp);
            this.kadri_page.Controls.Add(this.add_emp);
            this.kadri_page.Controls.Add(this.tabl_kadri);
            this.kadri_page.Location = new System.Drawing.Point(4, 22);
            this.kadri_page.Name = "kadri_page";
            this.kadri_page.Padding = new System.Windows.Forms.Padding(3);
            this.kadri_page.Size = new System.Drawing.Size(1157, 590);
            this.kadri_page.TabIndex = 0;
            this.kadri_page.Text = "Отдел кадров";
            this.kadri_page.UseVisualStyleBackColor = true;
            // 
            // search_twork
            // 
            this.search_twork.Location = new System.Drawing.Point(295, 483);
            this.search_twork.Name = "search_twork";
            this.search_twork.Size = new System.Drawing.Size(100, 20);
            this.search_twork.TabIndex = 14;
            this.search_twork.TextChanged += new System.EventHandler(this.search_twork_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(401, 483);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Таб. номер";
            // 
            // search_tabln
            // 
            this.search_tabln.Location = new System.Drawing.Point(481, 480);
            this.search_tabln.Name = "search_tabln";
            this.search_tabln.Size = new System.Drawing.Size(100, 20);
            this.search_tabln.TabIndex = 12;
            this.search_tabln.TextChanged += new System.EventHandler(this.search_tabln_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(154, 483);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Дата приема на работу";
            // 
            // search_nbrig
            // 
            this.search_nbrig.Location = new System.Drawing.Point(651, 480);
            this.search_nbrig.Name = "search_nbrig";
            this.search_nbrig.Size = new System.Drawing.Size(100, 20);
            this.search_nbrig.TabIndex = 10;
            this.search_nbrig.TextChanged += new System.EventHandler(this.search_nbrig_TextChanged);
            // 
            // search_dolzh
            // 
            this.search_dolzh.Location = new System.Drawing.Point(858, 483);
            this.search_dolzh.Name = "search_dolzh";
            this.search_dolzh.Size = new System.Drawing.Size(100, 20);
            this.search_dolzh.TabIndex = 9;
            this.search_dolzh.TextChanged += new System.EventHandler(this.search_dolzh_TextChanged);
            // 
            // search_fio
            // 
            this.search_fio.Location = new System.Drawing.Point(48, 483);
            this.search_fio.Name = "search_fio";
            this.search_fio.Size = new System.Drawing.Size(100, 20);
            this.search_fio.TabIndex = 8;
            this.search_fio.TextChanged += new System.EventHandler(this.search_fio_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(587, 480);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "№ бригады";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(787, 483);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Должность";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 486);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "ФИО";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 436);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Поиск";
            // 
            // delete_emp
            // 
            this.delete_emp.Location = new System.Drawing.Point(582, 525);
            this.delete_emp.Name = "delete_emp";
            this.delete_emp.Size = new System.Drawing.Size(100, 25);
            this.delete_emp.TabIndex = 3;
            this.delete_emp.Text = "Удалить";
            this.delete_emp.UseVisualStyleBackColor = true;
            this.delete_emp.Click += new System.EventHandler(this.delete_emp_Click);
            // 
            // edit_emp
            // 
            this.edit_emp.Location = new System.Drawing.Point(357, 525);
            this.edit_emp.Name = "edit_emp";
            this.edit_emp.Size = new System.Drawing.Size(100, 25);
            this.edit_emp.TabIndex = 2;
            this.edit_emp.Text = "Редактировать";
            this.edit_emp.UseVisualStyleBackColor = true;
            this.edit_emp.Click += new System.EventHandler(this.edit_emp_Click);
            // 
            // add_emp
            // 
            this.add_emp.Location = new System.Drawing.Point(91, 525);
            this.add_emp.Name = "add_emp";
            this.add_emp.Size = new System.Drawing.Size(100, 25);
            this.add_emp.TabIndex = 1;
            this.add_emp.Text = "Добавить";
            this.add_emp.UseVisualStyleBackColor = true;
            this.add_emp.Click += new System.EventHandler(this.add_emp_Click);
            // 
            // tabl_kadri
            // 
            this.tabl_kadri.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabl_kadri.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_kadri.Location = new System.Drawing.Point(3, 3);
            this.tabl_kadri.MultiSelect = false;
            this.tabl_kadri.Name = "tabl_kadri";
            this.tabl_kadri.RowHeadersWidth = 62;
            this.tabl_kadri.Size = new System.Drawing.Size(986, 414);
            this.tabl_kadri.TabIndex = 0;
            // 
            // Main_tabControl
            // 
            this.Main_tabControl.Controls.Add(this.kadri_page);
            this.Main_tabControl.Controls.Add(this.kpp_page);
            this.Main_tabControl.Controls.Add(this.otchet_page);
            this.Main_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Main_tabControl.Location = new System.Drawing.Point(0, 0);
            this.Main_tabControl.Name = "Main_tabControl";
            this.Main_tabControl.SelectedIndex = 0;
            this.Main_tabControl.Size = new System.Drawing.Size(1165, 616);
            this.Main_tabControl.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 616);
            this.Controls.Add(this.Main_tabControl);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Отчет ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.otchet_page.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_otchet)).EndInit();
            this.kpp_page.ResumeLayout(false);
            this.kpp_page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kpp)).EndInit();
            this.kadri_page.ResumeLayout(false);
            this.kadri_page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kadri)).EndInit();
            this.Main_tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage otchet_page;
        private System.Windows.Forms.DataGridView tabl_otchet;
        private System.Windows.Forms.TabPage kpp_page;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox filter_napr;
        private System.Windows.Forms.TextBox filter_cheh;
        private System.Windows.Forms.TextBox filter_time;
        private System.Windows.Forms.TextBox filter_emp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button delete_kpp;
        private System.Windows.Forms.Button edit_kpp;
        private System.Windows.Forms.Button add_kpp;
        private System.Windows.Forms.DataGridView tabl_kpp;
        private System.Windows.Forms.TabPage kadri_page;
        private System.Windows.Forms.Button delete_emp;
        private System.Windows.Forms.Button edit_emp;
        private System.Windows.Forms.Button add_emp;
        private System.Windows.Forms.DataGridView tabl_kadri;
        private System.Windows.Forms.TabControl Main_tabControl;
        private System.Windows.Forms.TextBox search_twork;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox search_tabln;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox search_nbrig;
        private System.Windows.Forms.TextBox search_dolzh;
        private System.Windows.Forms.TextBox search_fio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label Объект;
        private System.Windows.Forms.TextBox search_ot_dolzh;
        private System.Windows.Forms.TextBox search_ot_brig;
        private System.Windows.Forms.TextBox search_ot_cheh;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox time2_tb;
        private System.Windows.Forms.TextBox time1_tb;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox time_work_obj;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox obj_otch_tb;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label часов;
        private System.Windows.Forms.TextBox time_work_brig;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox n_brig_otc;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox n_obj_tb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox otc_count_em;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

